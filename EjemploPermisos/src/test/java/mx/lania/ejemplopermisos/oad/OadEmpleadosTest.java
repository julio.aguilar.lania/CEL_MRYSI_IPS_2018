package mx.lania.ejemplopermisos.oad;

import java.util.List;
import mx.lania.ejemplopermisos.config.ConfiguracionDataSource;
import mx.lania.ejemplopermisos.config.ConfiguracionJpa;
import mx.lania.ejemplopermisos.entidades.Empleado;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

/**
 *
 * @author jaguilar
 */
@RunWith(SpringJUnit4ClassRunner.class)

@ContextConfiguration(loader = AnnotationConfigContextLoader.class, 
        classes = {ConfiguracionDataSource.class, ConfiguracionJpa.class})
@ActiveProfiles("pruebas")
public class OadEmpleadosTest {
    
    
    @Autowired
    OadEmpleados oadEmpleados;
    
    @Test
    public void cargarEmpleados() {
        List<Empleado> emps = oadEmpleados.findAll();
        Assert.assertNotNull("Lista nula", emps);
        Assert.assertEquals("Numero incorrecto de empleados", 3, emps.size());
        
        Empleado emp;
        emp = oadEmpleados.findOne(2);
        Assert.assertNotNull("Empleado nulo", emp);
        Assert.assertEquals("Nombre incorrecto", "Paco", emp.getNombreEmpleado());
        
        emp = oadEmpleados.findOne(1234);
        Assert.assertNull("Empleado debia ser nulo", emp);
    }
    
    @Test
    public void buscarEmpleados() {
        List<Empleado> emps;
        emps = oadEmpleados.findByNombreEmpleado("Luis");
        Assert.assertNotNull("Lista nula", emps);
        Assert.assertEquals("Numero incorrecto de empleados", 1, emps.size());
        
        emps = oadEmpleados.findByNombreEmpleado("Pa");
        Assert.assertNotNull("Lista nula", emps);
        Assert.assertEquals("Numero incorrecto de empleados", 0, emps.size());
    }
    
    @Test
    public void buscarEmpleadosPorDepartamento() {
        List<Empleado> emps;
        emps = oadEmpleados.getPorIdDepartamento(2);
        Assert.assertNotNull("Lista nula", emps);
        Assert.assertEquals("Numero incorrecto de empleados", 2, emps.size());
        
        emps = oadEmpleados.getPorIdDepartamento(100);
        Assert.assertNotNull("Lista nula", emps);
        Assert.assertEquals("Numero incorrecto de empleados", 0, emps.size());
    }
    
    @Test(expected = org.springframework.transaction.TransactionException.class)
    public void guardarEmpleadoVacio() {
        Empleado emp = new Empleado();
        oadEmpleados.save(emp);
    }
}
