package mx.lania.ejemplopermisos.oad;

import mx.lania.ejemplopermisos.entidades.Usuario;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author PCEL
 */
public interface OadUsuarios extends JpaRepository<Usuario, Integer>{
    Usuario findByEmail(String email);
    
}
