package mx.lania.ejemplopermisos.control;

import java.util.List;
import mx.lania.ejemplopermisos.entidades.Empleado;
import mx.lania.ejemplopermisos.entidades.Usuario;
import mx.lania.ejemplopermisos.oad.OadEmpleados;
import mx.lania.ejemplopermisos.oad.OadUsuarios;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author PCEL
 */
@RestController
public class ControladorEmpleados {
    
    @Autowired
    OadUsuarios oadUsuarios;
    
    @Autowired
    OadEmpleados oadEmpleados;
            
    @GetMapping("/usuarios")
    public List<Usuario> getUsuarios() {
        return oadUsuarios.findAll();
    }
    
    @PostMapping("/usuarios")
    public Usuario crearUsuario(@RequestBody Usuario usr) {
        oadUsuarios.save(usr);
        return usr;
    }
    
    @PutMapping("/usuarios/{id}")
    public Usuario actualizarUsuario(@RequestBody Usuario usr) {
        return null;
    }
    
    @GetMapping("/empleados")
    public List<Empleado> getEmpleados() {
        return oadEmpleados.findAll();
    }
    
    @GetMapping(value="/empleados", params = {"nombre"})
    public List<Empleado> buscarEmpleadosPorNombre(@RequestParam("nombre") String nombre) {
        return oadEmpleados.findByNombreEmpleado(nombre);
    }
    
    @GetMapping("/empleados/{id}")
    public Empleado getEmpleadoPorId(@PathVariable("id")Integer idEmpleado) {
        return oadEmpleados.findOne(idEmpleado);
    }
}






